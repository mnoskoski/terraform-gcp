resource "google_container_cluster" "cluster01" {
  name               = "kub-cluster-01"
  location           = var.zone
  initial_node_count = 3

  master_auth {
    username = var.user
    password = var.password

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

 #   labels = {
 #     foo = ""
 #   }
#
#    tags = ["foo", "bar"]
  }

  timeouts {
    create = "10m"
    update = "10m"
  }
}