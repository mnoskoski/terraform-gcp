provider "google" {
  credentials = "/Users/marcelo/Documents/projetos/gavb/terraform/terraform-gcp/terraform-gke/credentials/credentials.json"
  project = "topazio"
  region = " southamerica-east-1"
}

terraform {
 backend "gcs" {
  bucket = "poc-bucket-terraform"
     prefix = "terraform/state"
 }
}