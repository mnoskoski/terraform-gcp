variable "zone"{
    type =  string
    default = "us-central1-a"
}

variable "image"{
    type =  string
    default = "debian-cloud/debian-10"
}

variable "user"{
    type = string
    default = "svc_k8s"
}

variable "password"{
    type = string
    default = "$(password_k8s_user)"
}