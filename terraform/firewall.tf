resource "google_compute_firewall" "fw-central1" {
  name    = "fw-central1"
  network = google_compute_network.vpc-central1.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "22", "8080"]
  }
  source_ranges = ["0.0.0.0/0"]
  source_tags = ["web-central1"]
}

resource "google_compute_firewall" "fw-asia-east1" {
  name    = "fw-asia-east1"
  network = google_compute_network.vpc-asia-east1.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "22", "8080"]
  }
  source_ranges = ["0.0.0.0/0"]
  source_tags = ["web-asia1"]
}