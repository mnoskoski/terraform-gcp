variable "vm_type"{
    type =  string
    default = "f1-micro"
}

variable "zone"{
    type =  string
    default = "us-central1-a"
}

variable "image"{
    type =  string
    default = "debian-cloud/debian-10"
}