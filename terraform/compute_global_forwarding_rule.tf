#create LoadBalance

resource "google_compute_global_forwarding_rule" "lb-terraform" {
  name       = "lb-terraform-app"
  target     = google_compute_target_http_proxy.default.id
  port_range = "80"
}

resource "google_compute_url_map" "default" {
  name            = "url-map-target-proxy"
  default_service = google_compute_backend_service.default.self_link
}

resource "google_compute_target_http_proxy" "default" {
  name        = "target-proxy"
  description = "a description"
  url_map     = google_compute_url_map.default.id
}

resource "google_compute_backend_service" "default" {
  name        = "backend"
  port_name   = "http"
  protocol    = "HTTP"
  timeout_sec = 10
  health_checks = [google_compute_http_health_check.default.id]
  
  backend {
    group          = google_compute_region_instance_group_manager.app-central1.instance_group
    balancing_mode = "UTILIZATION"
    capacity_scaler = 1.0
  }

  backend {
    group          = google_compute_region_instance_group_manager.app-asia1.instance_group
    balancing_mode = "UTILIZATION"
    capacity_scaler = 1.0
  }

}

resource "google_compute_http_health_check" "default" {
  name               = "check-backend"
  request_path       = "/"
  check_interval_sec = 1
  timeout_sec        = 1
}