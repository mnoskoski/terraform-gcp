resource "google_compute_region_instance_group_manager" "app-central1" {
  name = "app-central1"

  base_instance_name = "app"
  region             = "us-central1"
  target_size = 1 #notamanho do grupo
  version {
    instance_template = google_compute_instance_template.tf-project-final-us-central.self_link
  }
}

resource "google_compute_region_instance_group_manager" "app-asia1" {
  name = "app-asia1"
  base_instance_name = "app"
  region             = "asia-east1"
  target_size = 2
  version {
    instance_template = google_compute_instance_template.tf-project-final-asia-east.id
  }
}