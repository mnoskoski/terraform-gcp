resource "google_compute_instance_template" "tf-project-final-us-central" {
  #depends_on = google_compute_network.vpc-central1.id
  name        = "template-app-web-central"
  description = "This template is used to create app web servers."
  instance_description = "nginx app"
  machine_type          = var.vm_type
  
  // Create a new boot disk from an image
  disk {
    source_image = var.image
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = google_compute_network.vpc-central1.id
    subnetwork = google_compute_subnetwork.us-central-a.self_link
  }
}

resource "google_compute_instance_template" "tf-project-final-asia-east" {
  #depends_on = google_compute_network.vpc-asia-east1.id
  name        = "template-app-web-asia"
  description = "This template is used to create app web servers."

  labels = {
    environment = "dev"
  }
  instance_description = "nginx app"
  machine_type          = var.vm_type
  #can_ip_forward       = false

  // Create a new boot disk from an image
  disk {
    source_image = var.image
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = google_compute_network.vpc-asia-east1.self_link
    subnetwork = google_compute_subnetwork.asia-east1.self_link
  }
}