resource "google_compute_network" "vpc-central1" {
  name = "vpc-central1"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "us-central-a" {
  name = "us-central-a"
  ip_cidr_range= "192.168.10.0/24"
  region = "us-central1"
  network = google_compute_network.vpc-central1.self_link
}

resource "google_compute_subnetwork" "us-central-b" {
  name = "us-central-b"
  ip_cidr_range= "192.168.20.0/24"
  region = "us-central1"
  network = google_compute_network.vpc-central1.self_link
}

resource "google_compute_network" "vpc-asia-east1" {
  name = "vpc-asia-east1"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "asia-east1" {
  name = "asia-east1"
  ip_cidr_range= "192.168.20.0/24"
  region = "asia-east1"
  network = google_compute_network.vpc-asia-east1.self_link
}