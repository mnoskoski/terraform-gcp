provider "google"{
  project = "lab01-207708"
  region = " southamerica-east-1"
}

terraform {
 backend "gcs" {
     bucket = "cap-bucket-terraform"
     prefix = "terraform/state"
 }
}